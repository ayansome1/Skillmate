package com.example.this_is_ayan.skillmate;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by this_is_ayan on 07-04-2016.
 */
public class CustomJsonObjectRequest extends JsonObjectRequest
{
    public CustomJsonObjectRequest(int method, String url, JSONObject jsonRequest,Response.Listener listener, Response.ErrorListener errorListener)
    {
        super(method, url, jsonRequest, listener, errorListener);
    }

    @Override
    public Map getHeaders() throws AuthFailureError {
        Map headers = new HashMap();
        headers.put("x-access-token","eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiJ0ZXN0ZGF0ZSIsImdjbUlkIjoicGFzc3dvcmQiLCJpYXQiOjE0NjAzMDEyMTcsImV4cCI6MTQ2MDM4NzYxN30.SvDKT4BcWlWdVZ8WIm223q-VSsdw2hI_JCTrDG9PC3w");
        return headers;
    }

}
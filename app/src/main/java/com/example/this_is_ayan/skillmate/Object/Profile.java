package com.example.this_is_ayan.skillmate.Object;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by this_is_ayan on 08-04-2016.
 */
public class Profile
{
    private double distance;
    private boolean sex,isFav,showOnlyInitialOfNameFlag,maskMyProfilePhotoFlag,makeMePopularFlag;
    private String profileId,age,name,teach_id,teachSkillName,teachDescription,teachPortfolioLink;
    private int totalTrustScore,teachAvgRating,teachFees,teachAvailability;
    private List<String> profilePhotos,teachPhotoGallery;

   // private String profilePhotos;

    public Profile()
    {
        super();
        profilePhotos=new ArrayList<String>();
    }

    public double getDistance()
    {
        return distance;
    }
    public void setDistance(double distance)
    {
        this.distance=distance;
    }

    public boolean getSex()
    {
        return  sex;
    }
    public void setSex(boolean sex)
    {
        this.sex=sex;
    }

    public boolean getisFav()
    {
        return  isFav;
    }
    public  void setisFav(boolean isFav)
    {
        this.isFav=isFav;
    }

    public boolean getshowOnlyInitialOfNameFlag()
    {
        return  showOnlyInitialOfNameFlag;
    }
    public  void setShowOnlyInitialOfNameFlag(boolean showOnlyInitialOfNameFlag)
    {
        this.showOnlyInitialOfNameFlag=showOnlyInitialOfNameFlag;
    }

    public boolean getmaskMyProfilePhotoFlag()
    {
        return  maskMyProfilePhotoFlag;
    }
    public  void setMaskMyProfilePhotoFlag(boolean maskMyProfilePhotoFlag)
    {
        this.maskMyProfilePhotoFlag=maskMyProfilePhotoFlag;
    }

    public boolean getmakeMePopularFlag()
    {
        return  makeMePopularFlag;
    }
    public  void setMakeMePopularFlag(boolean makeMePopularFlag)
    {
        this.makeMePopularFlag=makeMePopularFlag;
    }

    public String getProfileId()
    {
        return profileId;
    }
    public  void setProfileId(String profileId)
    {
        this.profileId=profileId;
    }

    public String getAge()
    {
        return age;
    }
    public void setage(String age)
    {
        this.age=age;
    }

    public  String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name=name;
    }

    public int getTotalTrustScore()
    {
        return totalTrustScore;
    }
    public void setTotalTrustScore(int totalTrustScore)
    {
        this.totalTrustScore= totalTrustScore;
    }

    public List<String> getProfilePhotos()
    {
        return profilePhotos;
    }
    public  void setProfilePhotos(List<String> profilePhotos)
    {
        this.profilePhotos=profilePhotos;
    }

    public String getTeach_id()
    {
        return teach_id;
    }
    public  void setTeach_id(String teach_id)
    {
        this.teach_id=teach_id;
    }

    public String getTeachSkillName()
    {
        return teachSkillName;
    }
    public  void setTeachSkillName(String teachSkillName)
    {
        this.teachSkillName=teachSkillName;
    }

    public String getTeachDescription()
    {
        return teachDescription;
    }
    public  void setTeachDescription(String teachDescription)
    {
        this.teachDescription=teachDescription;
    }

    public String getTeachPortfolioLink()
    {
        return teachPortfolioLink;
    }
    public  void setTeachPortfolioLink(String teachPortfolioLink)
    {
        this.teachPortfolioLink=teachPortfolioLink;
    }

    public int getTeachAvgRating()
    {
        return teachAvgRating;
    }
    public void setTeachAvgRating(int teachAvgRating)
    {
        this.teachAvgRating= teachAvgRating;
    }

    public int getTeachFees()
    {
        return teachFees;
    }
    public void setTeachFees(int teachFees)
    {
        this.teachFees= teachFees;
    }

    public int getTeachAvailability()
    {
        return teachAvailability;
    }
    public void setTeachAvailability(int teachAvailability)
    {
        this.teachAvailability= teachAvailability;
    }

    public List<String> getTeachPhotoGallery()
    {
        return teachPhotoGallery;
    }
    public  void setTeachPhotoGallery(List<String> teachPhotoGallery)
    {
        this.teachPhotoGallery=teachPhotoGallery;
    }



}

package com.example.this_is_ayan.skillmate;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.this_is_ayan.skillmate.Object.Profile;
import com.example.this_is_ayan.skillmate.flingswipe.FlingCardListener;
import com.example.this_is_ayan.skillmate.flingswipe.SwipeFlingAdapterView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements FlingCardListener.ActionDownInterface
{
    ProgressBar progress;

    TextView output;
    String loginURL;
    String data = "";
    JSONObject result, profileJSONObject, teachJSONObject;
    JSONArray results, profilePhotos, teachPhotoGallery;
    //String profilePhotosArray[];
    List<String> profilePhotosList, teachPhotoGalleryList;
    Profile p;
    int resultsLength, i, j;
    ListView listView;
    public static AdapterProfile adapter;
    ArrayList<Profile> listProfile;//  = new ArrayList<Profile>();
    private SwipeFlingAdapterView flingContainer;
    public static AdapterProfile.ViewHolder holder;
    int start;
    CustomJsonObjectRequest jor;


    RequestQueue requestQueue;

    public static void removeBackground() {


        holder.background.setVisibility(View.GONE);
        adapter.notifyDataSetChanged();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        start=0;
        progress=(ProgressBar)findViewById(R.id.progress_bar);
        //progress.setVisibility(View.VISIBLE);
        flingContainer = (SwipeFlingAdapterView) findViewById(R.id.frame);


        listProfile = new ArrayList<Profile>();
        adapter= new AdapterProfile (listProfile,MainActivity.this);

        loginURL = "http://test.skillmate.co.in/v1/user?skillName=aerobics&id=145452999180469&category=learn&start="+start;

        requestQueue = Volley.newRequestQueue(this);

        jor = new CustomJsonObjectRequest(Request.Method.GET, loginURL, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        System.out.println(response.toString());

                        try {
                            result = response.getJSONObject("result");
                            results = result.getJSONArray("results");
                            resultsLength = results.length();
                            System.out.println("results length " + resultsLength);


                            for (i = 0; i < resultsLength; i++) {
                                try {
                                    p = new Profile();
                                    profileJSONObject = results.getJSONObject(i);
                                    p.setDistance(profileJSONObject.getDouble("distance"));
                                    p.setSex(profileJSONObject.getBoolean("sex"));
                                    p.setisFav(profileJSONObject.getBoolean("isFav"));
                                    p.setShowOnlyInitialOfNameFlag(profileJSONObject.getBoolean("showOnlyInitialOfNameFlag"));
                                    p.setMaskMyProfilePhotoFlag(profileJSONObject.getBoolean("maskMyProfilePhotoFlag"));
                                    p.setMakeMePopularFlag(profileJSONObject.getBoolean("makeMePopularFlag"));
                                    p.setProfileId(profileJSONObject.getString("profileId"));
                                    p.setage(profileJSONObject.getString("age"));
                                    p.setName(profileJSONObject.getString("name"));
                                    p.setTotalTrustScore(profileJSONObject.getInt("totalTrustScore"));
                                    profilePhotos = profileJSONObject.getJSONArray("profilePhotos");
                                    profilePhotosList = new ArrayList<String>();
                                    for (j = 0; j < profilePhotos.length(); j++) {
                                        profilePhotosList.add(profilePhotos.getString(j));
                                    }
                                    p.setProfilePhotos(profilePhotosList);
                                    teachJSONObject = profileJSONObject.getJSONObject("teach");
                                    p.setTeachSkillName(teachJSONObject.getString("skillName"));
                                    p.setTeach_id(teachJSONObject.getString("_id"));

                                    try {
                                        p.setTeachPortfolioLink(teachJSONObject.getString("portfolioLink"));
                                    } catch (JSONException x) {
                                        p.setTeachPortfolioLink("none");
                                    }

                                    try {
                                        p.setTeachDescription(teachJSONObject.getString("description"));
                                    } catch (JSONException x) {
                                        p.setTeachDescription("none");
                                    }

                                    try {
                                        teachPhotoGallery = teachJSONObject.getJSONArray("photoGallery");
                                        teachPhotoGalleryList = new ArrayList<String>();
                                        for (j = 0; j < teachPhotoGallery.length(); j++) {
                                            teachPhotoGalleryList.add(teachPhotoGallery.getString(j));
                                        }
                                        if (teachPhotoGalleryList.size() == 0)
                                            teachPhotoGalleryList.add("none");
                                        p.setTeachPhotoGallery(teachPhotoGalleryList);
                                    } catch (JSONException x) {
                                        teachPhotoGalleryList = new ArrayList<String>();
                                        teachPhotoGalleryList.add("");
                                        p.setTeachPhotoGallery(teachPhotoGalleryList);
                                    }

                                    try {
                                        p.setTeachFees(teachJSONObject.getInt("fees"));
                                    } catch (JSONException x) {
                                        p.setTeachFees(0);
                                    }

                                    try {
                                        p.setTeachAvgRating(teachJSONObject.getInt("avgRating"));
                                    } catch (JSONException x) {
                                        p.setTeachAvgRating(0);
                                    }

                                    try {
                                        p.setTeachAvailability(teachJSONObject.getInt("availability"));
                                    } catch (JSONException x) {
                                        p.setTeachAvailability(0);
                                    }

                                    listProfile.add(p);


                           /*     System.out.println("\n\n************ i=" + i);
                                System.out.println("distance " + p.getDistance());
                                System.out.println("sex " + p.getSex());
                                System.out.println("isfav " + p.getisFav());
                                System.out.println("showOnlyInitialOfNameFlag " + p.getshowOnlyInitialOfNameFlag());
                                System.out.println("maskMyProfilePhotoFlag " + p.getmaskMyProfilePhotoFlag());
                                System.out.println("makeMePopularFlag " + p.getmakeMePopularFlag());
                                System.out.println("ProfileId " + p.getProfileId());
                                System.out.println("Age " + p.getAge());
                                System.out.println("name " + p.getName());
                                System.out.println("TotalTrustScore " + p.getTotalTrustScore());
                                System.out.println("first profile photo link " + p.getProfilePhotos().get(0));
                                System.out.println("teach skill name " + p.getTeachSkillName());
                                System.out.println("teach id " + p.getTeach_id());
                                System.out.println("teach avg rating " + p.getTeachAvgRating());
                                System.out.println("teach description " + p.getTeachDescription());
                                System.out.println("teach portfolio link " + p.getTeachPortfolioLink());
                                System.out.println("teach first photo gallery link " + p.getTeachPhotoGallery().get(0));
                                System.out.println("teach first photo gallery link : none");
                                System.out.println("teach fees " + p.getTeachFees());
                                System.out.println("teach availability " + p.getTeachAvailability());*/


                                } catch (JSONException f) {
                                    System.out.println("error in JSON fetching " + f.toString());
                                }
                            }
                            //adapter = new AdapterProfile(listProfile,MainActivity.this);
                            flingContainer.setAdapter(adapter);
                            progress.setVisibility(View.GONE);
                            adapter.notifyDataSetChanged();
                            //listView=(ListView)findViewById(R.id.listView);
                            //listView.setAdapter(adapter);



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println(error.toString());
                        Log.e("Volley", "Error");

                    }
                }
        );
        requestQueue.add(jor);


        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {
                listProfile.remove(0);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                // listProfile.remove(0);
                //adapter.notifyDataSetChanged();
                //Do something on the left!
                //You also have access to the original object.
                //If you want to use it just cast it (String) dataObject

            }

            @Override
            public void onRightCardExit(Object dataObject) {

                //   listProfile.remove(0);
                // adapter.notifyDataSetChanged();
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter)
            {

                if(resultsLength==0)
                    return;

               // if(itemsInAdapter==0)
                 //   return;
                else if(itemsInAdapter!=3)
                    return;

                start=start+10;
                loginURL = "http://test.skillmate.co.in/v1/user?skillName=aerobics&id=145452999180469&category=learn&start="+start;


                jor = new CustomJsonObjectRequest(Request.Method.GET, loginURL, null,
                        new Response.Listener<JSONObject>()
                        {
                            @Override
                            public void onResponse(JSONObject response)
                            {
                                System.out.println(response.toString());

                                try {
                                    result = response.getJSONObject("result");
                                    results = result.getJSONArray("results");
                                    resultsLength = results.length();
                                    System.out.println("results length "+resultsLength);


                                    for (i = 0; i < resultsLength; i++) {
                                        try {
                                            p = new Profile();
                                            profileJSONObject = results.getJSONObject(i);
                                            p.setDistance(profileJSONObject.getDouble("distance"));
                                            p.setSex(profileJSONObject.getBoolean("sex"));
                                            p.setisFav(profileJSONObject.getBoolean("isFav"));
                                            p.setShowOnlyInitialOfNameFlag(profileJSONObject.getBoolean("showOnlyInitialOfNameFlag"));
                                            p.setMaskMyProfilePhotoFlag(profileJSONObject.getBoolean("maskMyProfilePhotoFlag"));
                                            p.setMakeMePopularFlag(profileJSONObject.getBoolean("makeMePopularFlag"));
                                            p.setProfileId(profileJSONObject.getString("profileId"));
                                            p.setage(profileJSONObject.getString("age"));
                                            p.setName(profileJSONObject.getString("name"));
                                            p.setTotalTrustScore(profileJSONObject.getInt("totalTrustScore"));
                                            profilePhotos = profileJSONObject.getJSONArray("profilePhotos");
                                            profilePhotosList = new ArrayList<String>();
                                            for (j = 0; j < profilePhotos.length(); j++) {
                                                profilePhotosList.add(profilePhotos.getString(j));
                                            }
                                            p.setProfilePhotos(profilePhotosList);
                                            teachJSONObject = profileJSONObject.getJSONObject("teach");
                                            p.setTeachSkillName(teachJSONObject.getString("skillName"));
                                            p.setTeach_id(teachJSONObject.getString("_id"));

                                            try {
                                                p.setTeachPortfolioLink(teachJSONObject.getString("portfolioLink"));
                                            } catch (JSONException x) {
                                                p.setTeachPortfolioLink("none");
                                            }

                                            try {
                                                p.setTeachDescription(teachJSONObject.getString("description"));
                                            } catch (JSONException x) {
                                                p.setTeachDescription("none");
                                            }

                                            try {
                                                teachPhotoGallery = teachJSONObject.getJSONArray("photoGallery");
                                                teachPhotoGalleryList = new ArrayList<String>();
                                                for (j = 0; j < teachPhotoGallery.length(); j++) {
                                                    teachPhotoGalleryList.add(teachPhotoGallery.getString(j));
                                                }
                                                if (teachPhotoGalleryList.size() == 0)
                                                    teachPhotoGalleryList.add("none");
                                                p.setTeachPhotoGallery(teachPhotoGalleryList);
                                            } catch (JSONException x) {
                                                teachPhotoGalleryList = new ArrayList<String>();
                                                teachPhotoGalleryList.add("");
                                                p.setTeachPhotoGallery(teachPhotoGalleryList);
                                            }

                                            try {
                                                p.setTeachFees(teachJSONObject.getInt("fees"));
                                            } catch (JSONException x) {
                                                p.setTeachFees(0);
                                            }

                                            try {
                                                p.setTeachAvgRating(teachJSONObject.getInt("avgRating"));
                                            } catch (JSONException x) {
                                                p.setTeachAvgRating(0);
                                            }

                                            try {
                                                p.setTeachAvailability(teachJSONObject.getInt("availability"));
                                            } catch (JSONException x) {
                                                p.setTeachAvailability(0);
                                            }

                                            listProfile.add(p);





                                        } catch (JSONException f) {
                                            System.out.println("error in JSON fetching " + f.toString());
                                        }
                                    }
                                    flingContainer.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();



                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                System.out.println(error.toString());
                                Log.e("Volley", "Error");

                            }
                        }
                );






                if(resultsLength!=0 && itemsInAdapter==3)
                    requestQueue.add(jor);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onScroll(float scrollProgressPercent) {

                View view = flingContainer.getSelectedView();
                try
                {
                    view.findViewById(R.id.background).setAlpha(0);

                }
                catch (NullPointerException n)
                {
                    n.printStackTrace();
                }
                //    view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                //view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
            }
        });
                          /*  flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClicked(int itemPosition, Object dataObject) {

                                    View view = flingContainer.getSelectedView();
                                    view.findViewById(R.id.background).setAlpha(0);

                                    adapter.notifyDataSetChanged();
                                }
                            });*/






    }

    @Override
    public void onActionDownPerform() {
        Log.e("action", "bingo");
    }


    public class AdapterProfile extends BaseAdapter {
        // public List<Data> parkingList;
        private List<Profile> lProfile;

        public Context context;

        public  class ViewHolder {

            //  private boolean sex,isFav,showOnlyInitialOfNameFlag,maskMyProfilePhotoFlag,makeMePopularFlag;
            //  private String profileId,age,name,teach_id,teachSkillName,teachDescription,teachPortfolioLink;
            // private int totalTrustScore,teachAvgRating,teachFees,teachAvailability;
            // private List<String> profilePhoto1,teachPhotoGallery1;
            public  FrameLayout background;

            public TextView distance, sex, isFav, showOnlyInitialOfNameFlag, maskMyProfilePhotoFlag, makeMePopularFlag,
                    profileId, age, name, teach_id, teachSkillName, teachDescription, teachPortfolioLink,
                    totalTrustScore, teachAvgRating, teachFees, teachAvailability,
                    profilePhoto1, teachPhotoGallery1;
        }

        private AdapterProfile(List<Profile> apps, Context context) {
            this.lProfile = apps;
            this.context = context;
        }


        @Override
        public int getCount() {
            return lProfile.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View rowView = convertView;


            if (rowView == null) {

                LayoutInflater inflater = getLayoutInflater();
                rowView = inflater.inflate(R.layout.item, parent, false);
                // configure view holder
                holder = new ViewHolder();


                holder.distance=(TextView)rowView.findViewById(R.id.distance);
                holder.sex = (TextView) rowView.findViewById(R.id.sex);
                holder.isFav = (TextView) rowView.findViewById(R.id.isFav);
                holder.showOnlyInitialOfNameFlag = (TextView) rowView.findViewById(R.id.showOnlyInitialOfNameFlag);
                holder.maskMyProfilePhotoFlag = (TextView) rowView.findViewById(R.id.maskMyProfilePhotoFlag);
                holder.makeMePopularFlag = (TextView) rowView.findViewById(R.id.makeMePopularFlag);
                holder.profileId = (TextView) rowView.findViewById(R.id.profileId);
                holder.age = (TextView) rowView.findViewById(R.id.age);
                holder.name = (TextView) rowView.findViewById(R.id.name);
                holder.teach_id = (TextView) rowView.findViewById(R.id.teach_id);
                holder.teachSkillName = (TextView) rowView.findViewById(R.id.teachSkillName);
                holder.teachDescription = (TextView) rowView.findViewById(R.id.teachDescription);
                holder.teachPortfolioLink = (TextView) rowView.findViewById(R.id.teachPortfolioLink);
                holder.totalTrustScore = (TextView) rowView.findViewById(R.id.totalTrustScore);
                holder.teachAvgRating = (TextView) rowView.findViewById(R.id.teachAvgRating);
                holder.teachFees = (TextView) rowView.findViewById(R.id.teachFees);
                holder.teachAvailability = (TextView) rowView.findViewById(R.id.teachAvailability);
                holder.profilePhoto1 = (TextView) rowView.findViewById(R.id.profilePhoto1);
                holder.teachPhotoGallery1 = (TextView) rowView.findViewById(R.id.teachPhotoGallery1);

               // viewHolder.DataText = (TextView) rowView.findViewById(R.id.bookText);
                holder.background = (FrameLayout) rowView.findViewById(R.id.background);
                rowView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }
          //  viewHolder.DataText.setText(parkingList.get(position).getDescription() + "");
            holder.distance.setText("Distance: "+String.valueOf(lProfile.get(position).getDistance()));
            holder.sex.setText("sex: "+String.valueOf(lProfile.get(position).getSex()));
            holder.isFav.setText("isFav: " + String.valueOf(lProfile.get(position).getisFav()));
            holder.showOnlyInitialOfNameFlag.setText("showOnlyInitialOfNameFlag: "+String.valueOf(lProfile.get(position).getshowOnlyInitialOfNameFlag()));
            holder.maskMyProfilePhotoFlag.setText("maskMyProfilePhotoFlag: "+String.valueOf(lProfile.get(position).getmaskMyProfilePhotoFlag()));
            holder.makeMePopularFlag.setText("makeMePopularFlag: "+String.valueOf(lProfile.get(position).getmakeMePopularFlag()));
            holder.profileId.setText("profileId: "+lProfile.get(position).getProfileId());
            holder.age.setText("age: "+lProfile.get(position).getAge());
            holder.name.setText("name: "+lProfile.get(position).getName());
            holder.teach_id.setText("teach_id: "+lProfile.get(position).getTeach_id());
            holder.teachSkillName.setText("teachSkillName: "+lProfile.get(position).getTeachSkillName());
            holder.teachDescription.setText("teachDescription: "+lProfile.get(position).getTeachDescription());
            holder.teachPortfolioLink.setText("teachPortfolioLink: "+lProfile.get(position).getTeachPortfolioLink());
            holder.totalTrustScore.setText("totalTrustScore: "+String.valueOf(lProfile.get(position).getTotalTrustScore()));
            holder.teachAvgRating.setText("teachAvgRating: "+String.valueOf(lProfile.get(position).getTeachAvgRating()));
            holder.teachFees.setText("teachFees: "+String.valueOf(lProfile.get(position).getTeachFees()));
            holder.teachAvailability.setText("teachAvailability: "+String.valueOf(lProfile.get(position).getTeachAvailability()));
            holder.profilePhoto1.setText("profilePhoto: " + lProfile.get(position).getProfilePhotos().get(0));
            holder.teachPhotoGallery1.setText("teachPhotoGallery : "+lProfile.get(position).getTeachPhotoGallery().get(0));



            return rowView;
        }
    }
}